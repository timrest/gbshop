from django.contrib.auth.views import LoginView, LogoutView, TemplateView
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages, auth

from django.conf import settings
from .models import User, Avatar
from django.views.generic  import DetailView, CreateView, DeleteView, UpdateView
from django.urls import reverse, reverse_lazy
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.http import HttpResponseRedirect, Http404
from .forms import UserLoginForm, UserRegistrationForm


class Login(SuccessMessageMixin, UserPassesTestMixin, LoginView):
    """docstring for LoginView"""
    template_name = 'accounts/login.html'
    success_message = _('Вы вошли в систему')
    form_class = UserLoginForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Log in')
        return context

    def test_func(self):
        return not self.request.user.is_authenticated

    def handle_no_permission(self):
        return HttpResponseRedirect('/')

    def get_next_page(self):
        next_page = super().get_next_page()
        if next_page:
            messages.success(self.request, self.success_message)
        return next_page

    # def form_valid(self, form):
    #     user = self.request.user
    #     if user and user.is_active:



class Logout(LoginRequiredMixin, LogoutView):
    """docstring for logoutView"""
    success_message = _('Вы вышли из системы')

    def get_next_page(self):
        next_page = super().get_next_page()
        if next_page:
            messages.success(self.request, self.success_message)
        return next_page


def send_verify_email(user):
    # код
    # ссылка для подтверждения
    verify_link = reverse('accounts:verify', args=[user.email, user.activation_key])
    title = f'Подтверждение учетной записи {user.username}'
    message = f'Для подтверждения учетной записи {user.username} \
    на портале {settings.DOMAIN_NAME} перейдите по ссылке: \
    \n{settings.DOMAIN_NAME}{verify_link}'

    return send_mail(title, message, settings.EMAIL_HOST_USER, [user.email], fail_silently=False)


class CreateProfile(SuccessMessageMixin, CreateView):
    """docstring for RegisterProfile"""
    template_name = 'accounts/register.html'
    model = User
    form_class = UserRegistrationForm
    success_url = '/'
    success_message = _('На Ваш почтовый ящик, указанный при регистрации было выслано письмо с ссылкой для активации аккаунта')
    error_message = _('К сожалению, возникла проблема с отправкой Вам письма с кодом активации. Проверьте правильность введенного адреса и попробуйте заново')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _('Register')
        return context


    def form_valid(self, form):
        response = super().form_valid(form)
        user = form.save()
        print(user.first_name)
        if send_verify_email(user):
            print('Письмо отправлено')
            return response
        else:
            print('Ошибка отправки')
            messages.error(self.request, self.error_message)
            return HttpResponseRedirect('/')


class VerificationUser(TemplateView):
    template_name = 'accounts/verification.html'

    def get(self, request, *args, **kwargs):
        email = kwargs.get('email')
        activation_key = kwargs.get('activation_key')
        user = User.objects.get(email=email)
        print(email)
        print(activation_key)
        print(user.activation_key)
        print (user.is_activation_key_expired())
        print (user.activation_key_expires)
        if user.activation_key == activation_key and not user.is_activation_key_expired():
            print('Зашел?')
            user.is_active = True
            user.save()
            auth.login(request, user)
        return super().get(request, *args, **kwargs)
        # print('Конец')
        # raise Http404


class profileView(LoginRequiredMixin, DetailView):
    """docstring for AddView"""
    model = User

    def get_object(self, queryset=None):
        return self.request.user


class editProfileView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """docstring for AddView"""
    fields = ('username', 'first_name', 'last_name', 'email', 'description', 'password')
    form = UserChangeForm
    model = User
    # success_url =  '/auth/profile/'
    success_message = _('Ваши данные успешно изменены')

    def get_success_url(self):
        self.object.set_password(self.object.password)
        self.object.save()
        return reverse_lazy('accounts:profile')

    def get_object(self, queryset=None):
        return self.request.user

    def save(self, commit=True):
        self.object.set_password(self.object.password)
        self.object.save()
        return self.object


class createAvatarView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """docstring for createAvatarView"""
    fields = ('image', 'title')
    model = Avatar
    success_message = _('Аватар добавлен')

    def form_valid(self, form):
        form.instance.related_obj = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('accounts:profile')


class deleteAvatarView(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, DeleteView):
    """docstring for DelView"""
    model = Avatar
    success_message = _('Аватар был удален')

    def test_func(self):
        pk = self.kwargs.get('pk', 0)
        return self.request.user.pictures.filter(id=pk).exists()

    def get_success_url(self):
        return reverse('accounts:profile')

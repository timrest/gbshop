from django.contrib import admin
from django.contrib.auth.models import Group, Permission
from .models import User
from mainapp.admin import PictureInline

# Register your models here.

class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'is_active', )
    inlines = (PictureInline,)
    exclude = ('sort', 'active', 'description', 'last_login')

    def save(self):
        super(self).save()
        self.object.set_password(self.object.password)
        self.object.save()

admin.site.register(User, UserAdmin)

admin.site.unregister(Group)
admin.site.register(Group)
admin.site.register(Permission)
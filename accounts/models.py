from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from mainapp.models import Core, Picture
from django.utils.translation import gettext_lazy as _
# Create your models here.

class User(AbstractUser, Core):
    """ Модель User"""
    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')

    #is_manager = models.BooleanField(_('Менеджер'), default=False) #может управлять товарами магазина без админ панели
    activation_key = models.CharField(max_length=128, blank=True)
    # Период активации
    activation_key_expires = models.DateTimeField(default=timezone.now() +
                                                          timezone.timedelta(hours=48))

    def is_activation_key_expired(self):
        return timezone.now() > self.activation_key_expires



class Avatar(Picture):
    """ Модель Avatar"""
    class Meta:
        verbose_name = _('Аватар')
        verbose_name_plural = _('Аватары')
        proxy = True
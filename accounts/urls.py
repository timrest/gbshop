from django.urls import re_path, path
from .views import Login, Logout, CreateProfile, profileView, editProfileView, createAvatarView, deleteAvatarView, VerificationUser
from django.conf import settings
from django.conf.urls.static import static
import os
#app_name = os.path.dirname(__file__).rsplit('/', 1)[1]
from pathlib import PurePath
app_name = PurePath(os.path.dirname(__file__)).parts[-1]

#app_name = 'accounts'

urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(), name='logout'),
    path('profile/', profileView.as_view(), name='profile'),
    path('editprofile/',  editProfileView.as_view(), name='editProfile'),
    path('createprofile/',  CreateProfile.as_view(), name='createProfile'),
    path('createavatar/<int:related_obj>/',  createAvatarView.as_view(), name='createAvatar'),
    path('deleteAvatar/<int:pk>/',  deleteAvatarView.as_view(), name='deleteAvatar'),
    path('verify/<str:email>/<str:activation_key>/', VerificationUser.as_view(), name='verify')
]

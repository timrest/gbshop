from django.contrib import admin
from .models import Invoice, InvoicePosition

# Register your models here.

class InvoicePositionAdmin(admin.ModelAdmin):
    list_display = ('prod', 'price', 'active', 'inv')

admin.site.register(InvoicePosition, InvoicePositionAdmin)
admin.site.register(Invoice)
from django.shortcuts import render
from django.contrib.admin.models import ADDITION, LogEntry
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import F, Count
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  RedirectView, UpdateView)
from django.views.generic.base import ContextMixin
from django.views.generic.edit import FormView
from .models import Invoice, InvoicePosition


# Create your views here.

class basketDetailView(LoginRequiredMixin, SuccessMessageMixin, DetailView):
    """
    Отображение содержимого корзины :model:`mainapp.Invoice`
    """
    model = Invoice

    def get_object(self, queryset=None):
        object, created = Invoice.objects.get_or_create(customer=self.request.user, active=True)
        return object



class addInvoicePosition(LoginRequiredMixin, UpdateView):

    fields = ('count',)
    model = InvoicePosition
    success_url = reverse_lazy('basket:basket_detail')

    def get(self, request, *args, **kwargs):
        print('+++++++++++++++++++')
        invoice, created = Invoice.objects.get_or_create(customer=self.request.user, active=True) # создаем или получаем корзину пользователя
        invoice_position, created = InvoicePosition.objects.get_or_create(inv=invoice, prod_id=self.kwargs['pk'], active=True) # создаем или получаем строку заказа в корзине
        invoice_position.count += 1
        invoice_position.price = invoice_position.prod.prices.first().price or 0
        invoice_position.save()
        return HttpResponseRedirect(self.success_url)


    def form_invalid(self, form):
        print(form.errors)


    def form_valid(self, form):
        # count = form.instance.count + 1
        # print(count)
        # invoice, created = Invoice.objects.get_or_create(customer=self.request.user, active=True) # создаем или получаем корзину пользователя
        # invoice_position, created = InvoicePosition.objects.get_or_create(inv=invoice, prod_id=self.kwargs['pk'], active=True) # создаем или получаем строку заказа в корзине
        # invoice_position.count += 1
        self.object.count += 1
        return super().form_valid(form)


class minusInvoicePositionCount(LoginRequiredMixin, UpdateView):
    fields = ('count',)
    model = InvoicePosition
    success_url = reverse_lazy('basket:basket_detail')

    def get(self, request, *args, **kwargs):
        print('------------------------')
        invoice, created = Invoice.objects.get_or_create(customer=self.request.user, active=True) # создаем или получаем корзину пользователя
        invoice_position, created = InvoicePosition.objects.get_or_create(inv=invoice, prod_id=self.kwargs['pk'], active=True) # создаем или получаем строку заказа в корзине
        if invoice_position.count > 0:
            invoice_position.count -= 1
        invoice_position.save()
        return HttpResponseRedirect(self.success_url)

    def form_valid(self, form):
        if self.object.count > 0:
            self.object.count -= 1
        return super().form_valid(form)


class dellInvoicePosition(LoginRequiredMixin, DeleteView):
    model = InvoicePosition

    def get_success_url(self):
        return reverse('basket:basket_detail')


class buy(LoginRequiredMixin, UpdateView):
    model = Invoice
    fields = ('active', )
    template_name = 'basket/invoice_detail.html'

    def get_success_url(self):
        return reverse('basket:basket_detail')

    def get_object(self, queryset=None):
        object, created = Invoice.objects.get_or_create(customer=self.request.user, active=True)
        print(object)
        return object

    def form_valid(self, form):
        obj = self.get_object()
        obj.active = False
        return super().form_valid(form)
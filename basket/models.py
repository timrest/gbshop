from django.db import models
from mainapp.models import Core, Product
from accounts.models import User
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum, F, FloatField, IntegerField

# Create your models here.

class Invoice(Core):

    class Meta:
        verbose_name = _('Счет')
        verbose_name_plural = _('Счета')

    customer = models.ForeignKey(User, verbose_name=_('invoices'), blank=True, null=True, related_name='invoices', on_delete=models.PROTECT)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    @property
    def get_total_price(self):
        return self.invoicepositions.total_price()

    @property
    def get_total_count(self):
        return self.invoicepositions.total_count()


class InvoicePositionManager(models.Manager):

    def get_active(self):
        return self.filter(active=True)

    def get_in_invoice(self, invoice):
        return self.filter(inv=invoice)

    def total_price(self):
        return self.aggregate(summ=Sum(F('price')*F('count'), output_field=FloatField()))['summ']

    def total_count(self):
        return self.aggregate(summ=Sum(F('count'), output_field=IntegerField()))['summ']

class InvoicePosition(Core):

    class Meta:
        verbose_name = _('Позиция счета')
        verbose_name_plural = _('Позиция счета')

    inv = models.ForeignKey(Invoice, verbose_name=_('invoicepositions'), null=False, related_name='invoicepositions', on_delete=models.PROTECT)
    prod = models.ForeignKey(Product, null=False, on_delete=models.PROTECT)
    price = models.DecimalField(max_digits=10, default=0, decimal_places=0)
    count = models.PositiveIntegerField(default=0, null=False, blank=True)
    object = InvoicePositionManager()


    @property
    def total_price(self):
        return float(int(self.count or 0) * float(self.price or 0))



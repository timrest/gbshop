from django.urls import re_path, path
from .views import addInvoicePosition, basketDetailView, minusInvoicePositionCount, dellInvoicePosition, buy
from django.conf import settings
from django.conf.urls.static import static
import os

from pathlib import PurePath
app_name = PurePath(os.path.dirname(__file__)).parts[-1]


urlpatterns = [
    path('invoice/', basketDetailView.as_view(), name='basket_detail'),
    path('addtobasket/<int:pk>/', addInvoicePosition.as_view(), name='addinvoiceposition'),
    path('minustobasket/<int:pk>/', minusInvoicePositionCount.as_view(), name='minusInvoicePositionCount'),
    path('dellInvoicePosition/<int:pk>/', dellInvoicePosition.as_view(), name='dellInvoicePosition'),
    path('buy', buy.as_view(), name = 'buy')
]

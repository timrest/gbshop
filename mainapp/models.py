from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Core(models.Model):
    """ Базовый класс моделей"""
    title = models.CharField(_('Название'), max_length=255, blank=True, null=True)
    description = models.TextField(_('Описание'), blank=True, null=True)
    sort = models.IntegerField(_('Сортировка'), default=0, blank=True, null=True)
    active = models.BooleanField(_('Активно'), default=False)

    class Meta:
        ordering = ['sort']

    def __str__(self):
        return f'{self.title}'

    @property
    def verbose_name(self):
        return self._meta.verbose_name

    @property
    def verbose_name_plural(self):
        return self._meta.verbose_name_plural


class ProductCategory(Core):
    """Категория товара"""
    class Meta:
        verbose_name = _('Продуктовая категория')
        verbose_name_plural = _('Продуктовые категории')


class ProductManufacturer(Core):
    """Производитель товара"""
    class Meta:
        verbose_name = _('Производитель')
        verbose_name_plural = _('Производители')


class Product(Core):
    """Продукт/товар"""
    class Meta:
        verbose_name = _('Продукт')
        verbose_name_plural = _('Продукты')

    category = models.ForeignKey(ProductCategory, null=True, on_delete=models.SET_NULL)
    manufacturer = models.ForeignKey(ProductManufacturer, null=True, on_delete=models.SET_NULL)


class PriceCurrency(Core):
    """Класс валюты"""
    class Meta:
        verbose_name = _('Валюта')
        verbose_name_plural = _('Валюты')


class ProductPrice(Core):
    """Цена товара"""
    price = models.DecimalField(_('Цена'), default=0, decimal_places=0, max_digits=20)
    currency = models.ForeignKey(PriceCurrency, null=True, on_delete=models.SET_NULL)
    prod = models.ForeignKey(Product, verbose_name=_('Цены'), related_name='prices', blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Цена на продукт')
        verbose_name_plural = _('Цены на продукты')


class Picture(Core):
    """Картинки для объектов"""
    class Meta:
        verbose_name = _('Картинка')
        verbose_name_plural = _('Картинки')

    image=models.ImageField(upload_to='images')
    related_obj=models.ForeignKey(Core, verbose_name=_('pictures'), blank=True, null=True, related_name='pictures', on_delete=models.CASCADE)


class Contact(Core):
    """Контакты"""

    class Meta:
        verbose_name = _('Контакт')
        verbose_name_plural = _('Контакты')

    phone = models.CharField(_('Телефон'), max_length=14)
    email = models.EmailField(_('Email'), max_length=64, null=True, blank=True)
    address = models.CharField(_('Адрес'), max_length=255)
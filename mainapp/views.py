from django.shortcuts import render_to_response
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from .models import Product, Picture, ProductCategory, ProductManufacturer, ProductPrice, Contact
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView, View
from django.utils.translation import ugettext_lazy as _


class indexView(ListView):
    """класс для представления index_view"""
    template_name = 'mainapp/index.html'
    model = Product

    def get_context_data(self, **kwargs):
        response = super().get_context_data(**kwargs)
        # response.update({'main_menu': main_menu})
        return response



class productDetailView(DetailView):
    """класс для представления product_detail_view"""
    model = Product
    template_name = 'mainapp/includes/inc-product-detail-view.html'

    def get_context_data(self, **kwargs):
        response = super().get_context_data(**kwargs)
        response['categories']= ProductCategory.objects.all()
        obj = response.get('object', None)
        obj_id = getattr(obj, 'id', 0)
        cat_id = getattr(obj, 'category', 0)

        product_list = Product.objects.exclude(id=obj_id).filter(category=cat_id).order_by('sort')

        response.update({'object_list': product_list })
        return response

class productCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):

    fields = '__all__'
    model = Product
    success_message = _('Продукт успешно добавлен')

    # def get_form_kwargs(self):
    #     form_kwargs = super().get_form_kwargs()
    #     form_kwargs['initial'] = {}
    #     form_kwargs['initial']['category'] = self.kwargs.get['cat_id']
    #     return form_kwargs

    def get_success_url(self):
        return reverse_lazy('mainapp:product_detail',args=(self.object.id or 0,))

class productEditView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    """docstring for AddView"""
    fields = '__all__'
    model = Product
    success_message = _('Данные продукта успешно изменены')

    def get_success_url(self):
        return reverse_lazy('mainapp:product_detail',args=(self.object.id or 0,))


class productPictureCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    """docstring for createAvatarView"""
    fields = ('image', 'title', 'active')
    model = Picture
    success_message = _('Изображение товара успешно добавлено')

    def form_valid(self, form):
        form.instance.related_obj_id = self.kwargs.get('related_obj')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('mainapp:product_detail',args=(self.object.related_obj.id or 0,))

    def get_context_data(self, **kwargs):
        response = super().get_context_data(**kwargs)
        response['related_obj']= self.kwargs.get('related_obj')
        return response


class productPictureDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    """docstring for DelView"""
    model = Picture
    success_message = _('Изображение товара было удалено')

    def get_success_url(self):
        return reverse_lazy('mainapp:product_detail',args=(self.object.related_obj.id or 0,))

#def index_view(request):
#    context = {'main_menu': main_menu, 'products': Product.objects.all()}
#    return render_to_response('mainapp/index.html', context)

#def products_view(request):
#    context = {'main_menu': main_menu, 'products': Product.objects.all()}
#    return render_to_response('mainapp/products-page.html', context)

class contactListView(ListView):
    template_name = 'mainapp/contact.html'
    model = Contact

    def get_context_data(self, **kwargs):
        response = super().get_context_data(**kwargs)
        #response.update({'main_menu': main_menu})
        return response

#def contact_view(request):
#    context = {'main_menu': main_menu, 'contacts': Contact.objects.all()}
#    return render_to_response('mainapp/contact.html', context)


class productListView(ListView):
    """класс для представления products_view"""
    # template_name = 'mainapp/product_detail.html'
    model = Product
    paginate_by = 3

    def get_context_data(self, **kwargs):
        response = super().get_context_data(**kwargs)
        response['categories']= ProductCategory.objects.all()
        return response

    def get_queryset(self):
        response = super().get_queryset()
        if hasattr(self, 'filter'):
            response = response.filter(**self.filter)
        # if 'search' in self.request.GET:
        #     search = self.request.GET.get('search', '')
        #     if search: # выдаю ВСЕ варианты поиска в категории ИЛИ в имени товара ИЛИ в его описании, оператор "|", склеивающий три запроса вместе
        #         response = (response.filter(**{'category__title__icontains': search}) |
        #                     response.filter(**{'title__icontains': search}) |    # icontains НЕ работает для sqllite
        #                     response.filter(**{'title__contains': search.capitalize() }) |
        #                     response.filter(**{'description__icontains': search}))
        return response

    def get(self, request, *args, **kwargs):
        pk = kwargs.pop('pk', None)
        if pk:
            self.filter = {'category': pk}
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        search= self.request.POST.get('search')
        if search: # глупо, но я передаю полученный запрос по каналу GET в контроллер вида allView
            self.filter = {'title__contains': search}
        return super().get(request, *args, **kwargs)


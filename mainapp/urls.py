from django.urls import re_path, path
from .views import indexView, contactListView, productListView, productDetailView, productCreateView, productEditView, productPictureCreateView, productPictureDeleteView
from django.conf import settings
from django.conf.urls.static import static
import os
#app_name = os.path.dirname(__file__).rsplit('/', 1)[1]
app_name = 'mainapp'

urlpatterns = [
    path('products/category/<int:pk>/', productListView.as_view(), name='products_in_catogory'),
    path('products/<int:pk>/', productDetailView.as_view(), name='product_detail'),
    path('products/', productListView.as_view(), name='products'),
    # path('search/', searchView.as_view(), name='search'),
    path('addnewproduct', productCreateView.as_view(), name='create_product'),
    path('addnewproduct/category/<int:cat_id>/',  productCreateView.as_view(), name='create_product'),
    path('editproduct/<int:pk>/', productEditView.as_view(), name='edit_product'),
    path('createprodpicture/<int:related_obj>/',  productPictureCreateView.as_view(), name='create_product_picture'),
    path('deleteprodpicture/<int:pk>/',  productPictureDeleteView.as_view(), name='delete_product_picture'),
    path('contacts', contactListView.as_view(), name='contacts'),
    re_path(r'^$', indexView.as_view(), name='index'),
    path(' ', indexView.as_view(), name='index'),
]

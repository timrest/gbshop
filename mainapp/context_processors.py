from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from basket.models import Invoice


def main_menu(request):
    main_menu = []
    if request.user.is_authenticated:
        main_menu += [
            {'get_url': 'accounts:profile', 'title': _(f'Hi, {request.user.first_name}')},
        ]

    main_menu += [
        {'get_url': 'mainapp:index', 'title': _('HOME')},
        {'get_url': 'mainapp:products', 'title': _('PRODUCTS')},
        {'get_url': 'mainapp:contacts', 'title': _('CONTACTS')},
        {'get_url': 'about', 'title': _('ABOUT US')}
    ]

    if request.user.is_authenticated:
        main_menu += [
            {'get_url': 'accounts:logout', 'title': _('LOG OUT')}]
        if request.user.is_staff:
            main_menu += [
                {'get_url': 'admin:index', 'title': _('ADMIN')}
            ]
    else:
        main_menu += [{'get_url': 'accounts:login', 'title': _('LOG IN')}]
    return {'main_menu': main_menu, 'VERSION': settings.VERSION}


def basket(request):
    basket = []
    if request.user.is_authenticated:
        basket = Invoice.objects.filter(customer=request.user, active=True)

    return {
        'basket': basket,
    }

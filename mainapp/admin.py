from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import ProductCategory, Product, ProductManufacturer, ProductPrice, Contact, Picture, PriceCurrency
# Register your models here.

class PictureInline(admin.TabularInline):
    model = Picture
    fk_name = 'related_obj'
    fields = ('title', 'active', 'image')

class PictureAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_object_name', 'active', 'sort')
    fields = ('title', 'image', 'active', 'sort', 'description')

    def get_object_name(self, obj):
        return getattr(getattr(obj, 'related_obj', None), 'title', None) or ''
    get_object_name.short_description = _('Связанный объект')

class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')

admin.site.register(ProductCategory, ProductCategoryAdmin)

class PriceCurrencyAdmin (admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')

admin.site.register(PriceCurrency, PriceCurrencyAdmin)

class PriceInline(admin.TabularInline):
    model = ProductPrice
    fk_name = 'prod'
    fields = ('title', 'active', 'price', 'currency')

class PriceAdmin(admin.ModelAdmin):
    list_display = ('title', 'price','currency','active', 'sort')

admin.site.register(ProductPrice, PriceAdmin)

class ProductManufacturerAdmin(admin.ModelAdmin):
    list_display = ('title', 'active', 'sort')

admin.site.register(ProductManufacturer, ProductManufacturerAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'category', 'manufacturer', 'active', 'sort')
    inlines = (PictureInline, PriceInline)

admin.site.register(Product, ProductAdmin)



class ContactAdmin(admin.ModelAdmin):
    list_display = ('title', 'address', 'phone', 'email', 'active', 'sort')

admin.site.register(Contact, ContactAdmin)






